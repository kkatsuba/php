<?php

// Kiryl Katsuba 

// Lesson 6. Functions 

function cartResults($products) {
    $totalCost = 0;
    $totalCount = 0;
    foreach ($products as $key => $value) {
        $totalCost += $value['price'];
        $totalCount += $value['quantity'];
    }
    return array('cost' => $totalCost, 'count' => $totalCount);
}

$products = array(    
    array('name' => 'Телевизор', 'price' => '400', 'quantity' => 1),     
    array('name' => 'Телефон', 'price' => '300', 'quantity' => 3),     
    array('name' => 'Кроссовки', 'price' => '150', 'quantity' => 2), 
);

echo '<pre>';
print_r(cartResults($products));
echo '</pre>';

function quadraticEquation($a, $b, $c) {
    $disc = pow($b, 2) - 4 * $a * $c;
    
    if ($disc > 0) {
        $uderSqrt = sqrt(pow($b, 2) - 4 * $a * $c);
        $x1 = (-$b + $uderSqrt) / (2 * $a);
        $x2 = (-$b - $uderSqrt) / (2 * $a);

        return array('x1' => $x1, 'x2' => $x2);
    } else if ($disc == 0)  {
        return -$b / (2 * $a);
    } else if ($disc < 0) {
        echo false;
    }
}

$quadraticResult = quadraticEquation(4, 30, 2);

echo '<pre>';
if ($quadraticResult) {
    print_r($quadraticResult);
} else {
    echo 'Doesn\'t have results';
}
echo '</pre>';


function deleteNegatives($digits) {
    return array_filter($digits, function($value) {
        return $value >= 0;
    });
}

$digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95); 
$digits = deleteNegatives($digits);

echo '<pre>';
print_r($digits);
echo '</pre>';

function deleteNegativesByLink(&$digits) {
    $digits = array_filter($digits, function($value) {
        return $value >= 0;
    });
}

$digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95); 
deleteNegativesByLink($digits);
echo '<pre>';
print_r($digits);
echo '</pre>';

// Lesson 8. Forms
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <form action="/index.php" method="POST">
                
                Num1: <input name="numbers[]" value="0" /> <br/>
                Num2: <input name="numbers[]" value="0" /> <br/>
                Num3: <input name="numbers[]" value="0" /> <br/>
                Num4: <input name="numbers[]" value="0" /> <br/>
                Num5: <input name="numbers[]" value="0" /> <br/>
                Num6: <input name="numbers[]" value="0" /> <br/>
                Num7: <input name="numbers[]" value="0" /> <br/>
          
                <button type="submit" name="submit" value="submit-numbers">Submit</button>
            </form>
            
            <?php
                if ($_POST['submit'] === 'submit-numbers') {
                    $numbers = array();
                    foreach ($_POST['numbers'] as $value) {
                        array_push($numbers, intval($value));
                    }


                    $min = min($numbers);
                    $max = max($numbers);
                    $avg = array_sum($numbers) / count($numbers);

                    echo '</br> max = ' . $max . '; min = ' . $min . '; avg = ' . $avg;
                }
            ?>
            
            <hr/>
            
            <form action="/index.php" method="POST">
                
                Name: <input name="name" required/> <br/>
                Gender: <input type="radio" name="gender" value="m" required/> mister
                        <input type="radio" name="gender" value="w" required/> missis <br/>
          
                <button type="submit" name="submit" value="submit-user-info">Submit</button>
            </form>
            
            <?php
                if ($_POST['submit'] === 'submit-user-info') {
                    $fullGender = $_POST['gender'] == 'm'  ? 'mister' : 'missis';
                    $name = $_POST['name'];
                    
                    echo 'Greetings, ' . $fullGender . ' ' . $name;
                }
            ?>

        </div>
    </body>
</html>
