<?php

// Data types 
$a = 152; 
$b = '152'; 
$c = 'London';
$d = array(152); 
$e = 15.2; 
$f = false; 
$g = true;

echo var_dump($a, $b, $c, $d, $e, $f, $g);

$a = 5;
$b = 10;

echo '<div>' . $a . ' из ' . $b . ' студентов ... </div>';
echo "<div>{$a} из {$b} .....</div>";

$greetings = 'Good morning';
$ladies = 'ladies';
$gentlemans = 'and gentlemans';

echo $greetings . ', ' . $ladies . ' ' . $gentlemans . '</br>';

$firstArray = array(1, 2, 3, 4, 5);
$secondArray = array("A", "B", "C", "D", "E");
        
$firstArray[2] = "random_value";

array_shift($secondArray);

echo '</br>$firstArray[2] = ' . $firstArray[2] . '; $secondArray[2] = ' . $secondArray[2];

echo '</br>';
var_dump($firstArray);
echo '</br>';
var_dump($secondArray);

echo '</br>$firstArray.size = ' . count($firstArray) . '; $secondArray.size = ' . count($secondArray);

// Condition operators

$min = 10;
$max = 50;
$x = 50;

function checkRange($min, $max, $x) {
    echo '</br>';
    if ($min == $x || $max == $x) {
        echo '+-';
        return;
    }
    if ($x > $min && $x < $max) {
        echo '+';
    } else {
        echo '-';
    }
}

checkRange($min, $max, $x);

$a = 2;
$b = 4;
$c = 3;

$disc = pow($b, 2) - 4 * $a * $c;
echo $disc. '</br>';
if ($disc > 0) {
    $uderSqrt = sqrt(pow($b, 2) - 4 * $a * $c);
    $x1 = (-$b + $uderSqrt) / (2 * $a);
    $x2 = (-$b - $uderSqrt) / (2 * $a);
    
    echo 'disc == '. $disc . '; x1 = ' . $x1 . '; x2 = ' . $x2;
}
if ($disc == 0)  {
    $x1 = -$b / (2 * $a);
    echo 'disc == 0; x1 = ' . $x1;
}
if ($disc < 0) {
    echo 'Нет корней';
}

$avg = ($a + $b + $c) / 3;
if (array_search($avg, [$a, $b, $c])) {
    echo '</br>' . $avg;
} else {
    echo '</br> Ошибка';
}

// Loops
$count = 0;
for ($i = 1; $i < 26; $i++) {
    $count += $i;
}
echo '</br>' . $count;

$count = 0;
$i = 0;
while ($i < 25) {
    $count += ++$i;
}
echo '</br>' . $count;

$n = 10;
for ($i = 1; $i < $n + 1; $i++) {
    echo '</br>pow(' . $i .') = ' . pow($i, 2);
}

$menu = [
    'Button 10', 
    'Button 9', 
    'Button 8', 
    'Button 7', 
    'Button 6', 
    'Button 5', 
    'Button 4',
    'Button 3', 
    'Button 2', 
    'Button 1'
];

krsort($menu);

$menuHtml = '<ul>';
foreach ($menu as $value) {
    $menuHtml .= '  <li><a href="#">' . $value . '</a></li>';
};
$menuHtml .= '</ul>';

echo '</br><pre>' . $menuHtml . '</pre>';