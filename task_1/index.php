<?php
// Kiryl Katsuba
/* 
 * 1/17/2017
 */

echo 'Heey!!';

// 2 => 
$makerAdress = 'some adress';
$carColor = '#000000';
$waterTemperature;
$phoneModel;

$something1 = 3;
$something2 = 5;
$something3 = 8;

echo "<div>{$something1}  {$something2}  {$something3}</div>";

$somethingResult = $something1 + $something2 + $something3;

echo "<div>{$something1} + {$something2} + {$something3} = {$somethingResult}</div>";

$result = (2 + 6 + 2) / (5 - 1);

echo "<div>result = {$result}</div>";

$a = 1; 
$b = 2; 
$c; 
$d;

echo "<div>a = {$a}; b = {$b}</div>";

$c = $a;
$d = &$b;

echo "<div>c = {$c}; d = {$d}</div>";

$a = 3;
$b = 4;

echo "<div>a = {$a}; b = {$b}; c = {$c}; d = {$d}</div>";

define("SOME_CONSTAN", 41);
define('ANOTHER_CONSTANT', 33);

echo '' . SOME_CONSTAN . " + " . ANOTHER_CONSTANT . " = " . (SOME_CONSTAN + ANOTHER_CONSTANT);

// SOME_CONSTANT = 123;